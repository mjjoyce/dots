#!/bin/sh

CWD="$(pwd)/.."
timestamp=`date +%Y%m%d`

echo "----------------------"
echo " Installing dot files"
echo "----------------------"

# For any dot file named <something>.dot
for name in $(find "${CWD}" -name \*.dot)
do

	# Strip the file name w/o path for display and set the final install directory.
	BASENAME="$(basename ${name} .dot)"
	DEST="${HOME}/.${BASENAME}"

	echo "\n-------------------"
	echo "Installing $BASENAME..."

	# vim needs to be handled separately to insure files are copied and run correctly
	if [ $BASENAME == "vimrc" ]; then
		# Backup existing .vim stuff
		echo "\nBacking up existing vim config"
		for i in .vim .vimrc .gvimrc; do 
			echo "Checking $i..."

			# If the file exists and it isn't a sym-link, back it up
			if [ -e $HOME/$i ] && [ ! -L $HOME/$i ]; then
				echo "Backing up $i..."
				mv -f $HOME/$i $HOME/$i.backup.$timestamp
			fi
		done

		# Install the files!
		cp -f $CWD/vim/vimrc.dot $HOME/.vimrc
		cp -f $CWD/vim/gvimrc.dot $HOME/.gvimrc
		cp -f $CWD/vim/vim.bundles $HOME/.vim.bundles

		# Setup everything needed for Vundle!
		if [ ! -d $HOME/.vim/bundle/vundle ]; then
			echo "\nInstalling Vundle"
			git clone http://github.com/gmarik/vundle.git $HOME/.vim/bundle/vundle
		fi

		# Install plugins with Vundle
		#
		# The vim.bundles file that we just installed contains all the info that we need
		# to handle installing Vundle and various bundles. So, let's do that!
		vim -u $HOME/.vim.bundles +BundleInstall! +BundleClean +qall
	else
		# Handle file installs (Needed for .bashrc, etc).
		if [ -f "${DEST}" ]
		then
			# If the file already exists, back it up first! This will clobber a previous backup.
			echo "The $BASENAME file already exists. Backing up to $BASENAME.backup.$timestamp"
			mv -f "${DEST}" "${DEST}".backup.$timestamp
		fi

		echo "Copying to $DEST"
		cp -f "$name" "$DEST"
	fi
done

echo "\n-------------------"
echo "Installing Bins"

# Install binaries
if [ -e "${HOME}/bin" ]
then
    echo "Backing up ${HOME}/bin"
    mv -f "${HOME}/bin" "${HOME}/bin.backup.${timestamp}"
fi

echo "Copying to ${DEST}"
mkdir "${HOME}/bin" > /dev/null 2>&1
cp -rf "${CWD}/bin" "${HOME}"
    

echo ""
echo "----------------------"
echo "   Install Finished"
echo "----------------------"
